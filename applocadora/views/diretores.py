
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from applocadora.forms.Form_diretores import *
from applocadora.models.diretores import Diretores
from locadora.settings import *
from django.conf import settings


@login_required(login_url='/login/')
def add_diretores(request):
	
	if request.method == 'POST':

		form = Form_diretores(request.POST)

		if form.is_valid():
						
			diretores = Diretores()
			diretores.nome = request.POST["nome"]
			diretores.save()

			return redirect("/lista_diretores/")

	else:

		form = Form_diretores()
	
	return render(request, 'add_diretores.html', {
		"add_diretores":form
	})

@login_required(login_url='/login/')
def lista_diretores(request):

	return render(request, 'diretores.html', {
		"diretores":Diretores().get_all()
		})

@login_required(login_url='/login/')
def edita_diretores(request, id_diretores):	

	diretores = Diretores.objects.get(pk=id_diretores)

	if request.method == 'POST':

		form = Form_diretores(request.POST)

		if form.is_valid():
			
			diretores.nome = request.POST["nome"]
			diretores.save()

			return redirect("/lista_diretores/")

	else:

		dados_diretores = {
			'nome': diretores.nome, 
		}

		form = Form_diretores(initial=dados_diretores)
	
	return render(request, 'edita_diretores.html', {
		"edita_diretores":form,
		"id_diretores":id_diretores,
	})

@login_required(login_url='/login/')
def delete_diretores(request, id_diretores):

	Diretores.objects.filter(pk=id_diretores).delete()
	return redirect("/lista_diretores/")
