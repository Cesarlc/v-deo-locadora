#coding: utf-8

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from applocadora.forms.Form_login import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from applocadora.views.home import *

def autenticacao(request):

	if request.method == 'POST':

		form = Form_login(request.POST)

		if form.is_valid():

			username = request.POST["nome_pessoa"]
			password = request.POST["senha"]
			
			user = authenticate(username=username, password=password)

			if user is not None:

				if user.is_active:
					
					login(request, user)
					return redirect("/lista_filmes/")
			
			else:
				
				return redirect("/login/")

	else:

		form = Form_login() 

	return render(request, 'login.html', {
		"login":form
	})

def sair(request):
    
    logout(request)
    return HttpResponseRedirect("/login/")

