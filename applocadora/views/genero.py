
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from applocadora.forms.Form_genero import *
from applocadora.models.genero import Genero
from locadora.settings import *
from django.conf import settings

@login_required(login_url='/login/')
def add_genero(request):
	
	if request.method == 'POST':

		form = Form_genero(request.POST)

		if form.is_valid():
			
			genero = Genero()
			genero.nome = request.POST["nome"]
			genero.save()

			return redirect("/lista_genero/")

	else:

		form = Form_genero()
	
	return render(request, 'add_genero.html', {
		"add_genero":form
	})

@login_required(login_url='/login/')
def lista_genero(request):

	return render(request, 'genero.html', {
		"generos":Genero().get_all()
		})

@login_required(login_url='/login/')
def edita_genero(request, id_genero):	

	genero = Genero.objects.get(pk=id_genero)

	if request.method == 'POST':

		form = Form_genero(request.POST)

		if form.is_valid():
			
			genero.nome = request.POST["nome"]
			genero.save()

			return redirect("/lista_genero/")

	else:

		dados_genero = {
			'nome': genero.nome, 
		}

		form = Form_genero(initial=dados_genero)
	
	return render(request, 'edita_genero.html', {
		"edita_genero":form,
		"id_genero":id_genero,
	})

@login_required(login_url='/login/')
def delete_genero(request, id_genero):

	Genero.objects.filter(pk=id_genero).delete()
	return redirect("/lista_genero/")

