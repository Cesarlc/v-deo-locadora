from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from applocadora.forms.Form_bairro import *
from applocadora.models.bairro import Bairro
from locadora.settings import *
from django.conf import settings

@login_required(login_url='/login/')
def add_bairro(request):
	
	if request.method == 'POST':

		form = Form_bairro(request.POST)

		if form.is_valid():
						
			diretores = Bairro()
			diretores.nome = request.POST["nome"]
			diretores.save()

			return redirect("/add_clientes/")

	else:

		form = Form_bairro()
	
	return render(request, 'add_bairro.html', {
		"add_bairro":form
	})
