
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from applocadora.forms.Form_clientes import *
from applocadora.models.clientes import Clientes
from applocadora.models.emprestimo import Filmes_clientes
from locadora.settings import *
from django.conf import settings

@login_required(login_url='/login/')
def add_clientes(request):
	
	if request.method == 'POST':

		form = Form_clientes(request.POST)

		if form.is_valid():
						
			clientes = Clientes()
			clientes.nome = request.POST["nome"]
			clientes.cpf = request.POST["cpf"]
			clientes.rg = request.POST["rg"]
			clientes.telefone = request.POST["telefone"]
			clientes.rua = request.POST["rua"]
			clientes.numero = request.POST["numero"]
			clientes.cep = request.POST["cep"]
			clientes.bairro_id = request.POST["bairro"]
			clientes.save()

			return redirect("/lista_clientes/")

	else:

		form = Form_clientes()
	
	return render(request, 'add_clientes.html', {
		"add_clientes":form
	})

@login_required(login_url='/login/')
def lista_clientes(request):

	emprestimos = Clientes().valor_devido()

	return render(request, 'clientes.html', {
		"clientes":emprestimos

		})



@login_required(login_url='/login/')
def edita_clientes(request, id_clientes):	

	clientes = Clientes.objects.get(pk=id_clientes)

	if request.method == 'POST':

		form = Form_clientes(request.POST)

		if form.is_valid():
			
			clientes.nome = request.POST["nome"]
			clientes.cpf = request.POST["cpf"]
			clientes.rg = request.POST["rg"]
			clientes.telefone = request.POST["telefone"]
			clientes.rua = request.POST["rua"]
			clientes.numero = request.POST["numero"]
			clientes.cep = request.POST["cep"]
			clientes.bairro_id = request.POST["bairro"]
			clientes.save()

			return redirect("/lista_clientes/")

	else:

		dados_clientes = {
			'id': clientes.id,
			'nome': clientes.nome,
			'cpf': clientes.cpf,
			'rg': clientes.rg,
			'telefone': clientes.telefone,
			'rua': clientes.rua,
			'numero': clientes.numero,
			'cep': clientes.cep,
			'bairro': clientes.bairro_id, 
		}

		form = Form_clientes(initial=dados_clientes)
	
	return render(request, 'edita_clientes.html', {
		"edita_clientes":form,
		"id_clientes":id_clientes,
	})

@login_required(login_url='/login/')
def delete_clientes(request, id_clientes):

	Clientes.objects.filter(pk=id_clientes).delete()
	return redirect("/lista_clientes/")
