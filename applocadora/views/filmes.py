from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from applocadora.forms.Form_filmes import *
from applocadora.forms.Form_emprestimo import *
from applocadora.models.filmes import Filmes
from applocadora.models.diretores import Diretores
from applocadora.models.genero import Genero
from applocadora.models.emprestimo import Filmes_clientes
from locadora.settings import *
from django.conf import settings

@login_required(login_url='/login/')
def add_filmes(request):
	
	if request.method == 'POST':

		form = Form_filmes(request.POST)

		if form.is_valid():
						
			filmes = Filmes()
			filmes.nome = request.POST["nome"]
			filmes.descricao = request.POST["descricao"]
			filmes.numero_copias = request.POST["numero_copias"]
			filmes.diretores_id = request.POST["diretor"]
			filmes.genero_id = request.POST['genero']
			filmes.save()

			return redirect("/lista_filmes/")

	else:

		form = Form_filmes()
	
	return render(request, 'add_filmes.html', {
		"add_filmes":form
	})

@login_required(login_url='/login/')
def lista_filmes(request):

	return render(request, 'filmes.html', {
		"filmes":Filmes().get_all()
		})


@login_required(login_url='/login/')
def edita_filmes(request, id_filmes):	

	filmes = Filmes.objects.get(pk=id_filmes)

	if request.method == 'POST':

		form = Form_filmes(request.POST)

		if form.is_valid():
			
			filmes.nome = request.POST["nome"]
			filmes.descricao = request.POST["descricao"]
			filmes.numero_copias = request.POST["numero_copias"]
			filmes.diretores_id = request.POST["diretor"]
			filmes.genero_id = request.POST['genero']
			filmes.save()

			return redirect("/lista_filmes/")

	else:

		dados_filmes = {
			'nome': filmes.nome, 
			'descricao': filmes.descricao,
			'numero_copias': filmes.numero_copias,
			'diretor': filmes.diretores_id,
			'genero': filmes.genero_id,
		}

		form = Form_filmes(initial=dados_filmes)
	
	return render(request, 'edita_filmes.html', {
		"edita_filmes":form,
		"id_filmes":id_filmes,
	})

@login_required(login_url='/login/')
def delete_filmes(request, id_filmes):

	Filmes.objects.filter(pk=id_filmes).delete()
	return redirect("/lista_filmes/")

@login_required(login_url='/login/')
def verifica_filme(request, id_filmes):

	filmes = Filmes.objects.get(pk=id_filmes)

	if filmes.numero_copias != 0:

		return redirect("/aluga_filme/")

	else:

		return redirect("/emprestado/")

@login_required(login_url='/login/')
def aluga_filme(request):


	if request.method == 'POST':

		form = Form_emprestimo(request.POST)

		if form.is_valid():

			emprestimo = Filmes_clientes()		
			emprestimo.data_saida = request.POST["data_saida"]
			emprestimo.data_devolucao = request.POST["data_devolucao"]
			emprestimo.valor = request.POST["valor"]
			emprestimo.filmes_id = request.POST["filme"]
			emprestimo.clientes_id = request.POST['clientes']
			emprestimo.save()

			filmes = Filmes.objects.get(pk=emprestimo.filmes_id)

			filmes.numero_copias -= 1
			filmes.save()

			return redirect("/lista_emprestimos/")

	else:

		form = Form_emprestimo()
	
	return render(request, 'aluga_filme.html', {
		"aluga_filme":form
	})