from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from applocadora.forms.Form_emprestimo import *
from applocadora.forms.Form_retorno import *
from applocadora.forms.Form_pagamento import *
from applocadora.models.emprestimo import Filmes_clientes
from locadora.settings import *
from django.conf import settings
from datetime import datetime, date


@login_required(login_url='/login/')
def lista_emprestimos(request):

	return render(request, 'lista_emprestimos.html', {
		"emprestimos":Filmes_clientes().get_all()
		})

@login_required(login_url='/login/')
def lista_emprestimos_divida_cliente(request, id_cliente):

	return render(request, 'lista_emprestimos_divida_cliente.html', {
		"emprestimos":Filmes_clientes.objects.filter(clientes_id = id_cliente, valor__gt = 0).all()
		})

@login_required(login_url='/login/')
def emprestado(request):
	
	return render(request, 'emprestado.html', {
	
	})

def retorno_filme(request, id_emprestimo):

	emprestimo = Filmes_clientes.objects.get(pk=id_emprestimo)

	if request.method == 'POST':

		form = Form_retorno(request.POST)

		if form.is_valid():

			data = datetime.strptime(request.POST["data_retorno"], '%Y-%m-%d')

			data_devolver = emprestimo.data_devolucao

			data_devolver = datetime.combine(data_devolver,datetime.min.time())

			if data_devolver < data:

				multa = data.day - data_devolver.day
				emprestimo.valor += multa


			emprestimo.data_retorno = request.POST["data_retorno"]
			emprestimo.filmes_id = request.POST["filme"]
			emprestimo.clientes_id = request.POST['clientes']
			emprestimo.save()

			filmes = Filmes.objects.get(pk=emprestimo.filmes_id)

			filmes.numero_copias += 1
			filmes.save()


			return redirect("/lista_emprestimos/")

	else:

		data = {
			'filme' : emprestimo.filmes,
			'clientes' : emprestimo.clientes,
		}

		form = Form_retorno(initial=data)
	
	return render(request, 'retorno_filme.html', {
		"retorno_filme":form,
		"id_emprestimo": emprestimo.id
	})

@login_required(login_url='/login/')
def pagar(request, id_emprestimo):
	
	emprestimo = Filmes_clientes.objects.get(pk=id_emprestimo)

	if request.method == 'POST':

		form = Form_pagamento(request.POST)

		if form.is_valid():

			pagamento = emprestimo.valor - float(request.POST["valor"])

			emprestimo.valor = pagamento

			emprestimo.save()

			return redirect("/lista_clientes/")

	else:

		form = Form_pagamento()
	
	return render(request, 'pagar.html', {
		"pagar":form,
		"id_emprestimo" : emprestimo.id,
	})