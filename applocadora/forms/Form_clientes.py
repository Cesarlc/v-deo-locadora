#coding: utf-8 

from django import forms
from django.db.models import Q
from applocadora.models.bairro import Bairro
from applocadora.models.clientes import Clientes

class Form_clientes(forms.Form):
	
	id = forms.IntegerField(
		initial = 0,
		widget = forms.HiddenInput()
	)

	nome = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o nome do cliente...',
			'id':'nome'
		})
	)

	cpf = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o cpf do cliente...',
			'id':'cpf'
		})
	)

	rg = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o rg do cliente...',
			'id':'rg'
		})
	)

	telefone = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o telefone do cliente...',
			'id':'telefone'
		})
	)

	rua = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe a rua do cliente...',
			'id':'rua'
		})
	)

	numero = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o numero da casa do cliente...',
			'id':'numero'
		})
	)
	
	cep = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o cep do cliente...',
			'id':'cep'
		})
	)

	bairro = forms.ModelChoiceField(
		widget=forms.Select(attrs={
			'class':'form-control',
			'id':'Bairro'
		}),
		queryset=Bairro.objects.all(),
		empty_label="Selecione o bairro"
	)

	def clean_cpf(self):

		cpf = self.cleaned_data["cpf"]
		id = self.cleaned_data["id"]
		clientes = Clientes.objects.filter(Q(cpf=cpf), ~Q(id=id))
		if clientes.count() > 0:

			raise forms.ValidationError("Este CPF já existe!")

		return cpf

	def clean_rg(self):

		rg = self.cleaned_data["rg"]
		id = self.cleaned_data["id"]
		clientes = Clientes.objects.filter(Q(rg=rg), ~Q(id=id))

		if clientes.count() > 0:

			raise forms.ValidationError("Este RG já existe!")

		return rg
