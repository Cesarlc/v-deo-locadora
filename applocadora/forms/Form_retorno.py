#coding: utf-8 

from django import forms
from applocadora.models.filmes import Filmes
from applocadora.models.emprestimo import Filmes_clientes
from applocadora.models.clientes import Clientes

class Form_retorno(forms.Form):
	
	data_retorno = forms.CharField(
		max_length=100,
		widget=forms.DateInput(attrs={
			'class':'form-control',
			'placeholder':'Informe a data de retorno...',
			'id':'data_retorno',
			'type': 'date'
		})
	)

	filme = forms.ModelChoiceField(
		widget=forms.Select(attrs={
			'class':'form-control',
			'id':'filmes'
		}),
		queryset=Filmes.objects.all(),
		empty_label="Selecione o filme"
	)

	clientes = forms.ModelChoiceField(
		widget=forms.Select(attrs={
			'class':'form-control',
			'id':'clientes'
		}),
		queryset=Clientes.objects.all(),
		empty_label="Selecione um clientes"
	)