#coding: utf-8 

from django import forms
from applocadora.models.filmes import Filmes
from applocadora.models.clientes import Clientes

class Form_emprestimo(forms.Form):
	
	data_saida = forms.CharField(
		max_length=100,
		widget=forms.DateInput(attrs={
			'class':'form-control',
			'placeholder':'Informe a data do emprestimo...',
			'id':'data_saida',
			'type': 'date'
		})
	)

	data_devolucao = forms.CharField(
		max_length=100,
		widget=forms.DateInput(attrs={
			'class':'form-control',
			'placeholder':'Informe a data do emprestimo...',
			'id':'data_devolucao',
			'type': 'date'
		})
	)

	valor = forms.CharField(
		max_length=100,
		widget=forms.DateInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o valor emprestimo...',
			'id':'valor'
		})
	)

	filme = forms.ModelChoiceField(
		widget=forms.Select(attrs={
			'class':'form-control',
			'id':'filmes'
		}),
		queryset=Filmes.objects.all(),
		empty_label="Selecione o filme"
	)

	clientes = forms.ModelChoiceField(
		widget=forms.Select(attrs={
			'class':'form-control',
			'id':'clientes'
		}),
		queryset=Clientes.objects.all(),
		empty_label="Selecione um clientes"
	)