#coding: utf-8 

from django import forms

class Form_bairro(forms.Form):
	
	nome = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o nome do bairro...',
			'id':'nome'
		})
	)