#coding: utf-8 

from django import forms

class Form_genero(forms.Form):
	
	nome = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o genero...',
			'id':'nome'
		})
	)

