#coding: utf-8 

from django import forms

class Form_pagamento(forms.Form):
	
	valor = forms.FloatField(
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o valor a ser pago...',
			'id':'valor'
		})
	)