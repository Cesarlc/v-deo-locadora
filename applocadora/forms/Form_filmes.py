#coding: utf-8 

from django import forms
from applocadora.models.diretores import Diretores
from applocadora.models.genero import Genero

class Form_filmes(forms.Form):
	
	nome = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o nome do filme...',
			'id':'nome'
		})
	)

	descricao = forms.CharField(
		max_length=100,
		widget=forms.Textarea(attrs={
			'class':'form-control',
			'placeholder':'Informe a sinopse do filme...',
			'id':'descricao'
		})
	)

	numero_copias = forms.CharField(
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe o número de cópias do filme...',
			'id':'numero_copias'
		})
	)

	genero = forms.ModelChoiceField(
		widget=forms.Select(attrs={
			'class':'form-control',
			'id':'Genero'
		}),
		queryset=Genero.objects.all(),
		empty_label="Selecione um genero"
	)

	diretor = forms.ModelChoiceField(
		widget=forms.Select(attrs={
			'class':'form-control',
			'id':'Diretores'
		}),
		queryset=Diretores.objects.all(),
		empty_label="Selecione o diretor"
	)