#coding: utf-8 

from django import forms

class Form_login(forms.Form):
	
	nome_pessoa = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Informe seu nome...',
			'id':'nome_pessoa'
		})
	)
	
	senha = forms.CharField(
		max_length=100,
		widget=forms.TextInput(attrs={
			'type':'password',
			'class':'form-control',
			'placeholder':'Informe sua senha...',
			'id':'senha'
		})
	)
	

