# -*- coding: utf-8 -*-

from django.db import models
import datetime

class Genero (models.Model):

	class Meta:
		app_label = 'applocadora'

	nome = models.CharField(max_length=200)

	def get_all(self):
		return Genero.objects.filter().all()

	def __unicode__(self):
		return self.nome

