# -*- coding: utf-8 -*-

from django.db import models
from clientes import *
from filmes import *

class Filmes_clientes (models.Model):

	class Meta:
		app_label = 'applocadora'

	data_saida = models.DateField()
	data_devolucao = models.DateField()
	data_retorno = models.DateTimeField(null=True)
	valor = models.FloatField()
	filmes = models.ForeignKey(Filmes)
	clientes = models.ForeignKey(Clientes)

	def get_all(self):
		return Filmes_clientes.objects.filter(data_retorno=None).all()

	def __unicode__(self):
		return unicode(self.data_saida)