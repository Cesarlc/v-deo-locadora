# -*- coding: utf-8 -*-

from django.db import models
from endereco import *
import datetime

class Login (models.Model):

	class Meta:
		app_label = 'applocadora'

	nome = models.CharField(max_length=200)
	cpf = models.IntegerField()
	senha = models.CharField(max_length=10)