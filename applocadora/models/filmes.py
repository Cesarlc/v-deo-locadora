# -*- coding: utf-8 -*-

from django.db import models
from diretores import *
from genero import *
import datetime

class Filmes (models.Model):

	class Meta:
		app_label = 'applocadora'

	nome = models.CharField(max_length=200)
	descricao = models.TextField(max_length=1000)
	status = models.CharField(max_length=100)
	numero_copias = models.IntegerField()
	genero = models.ForeignKey(Genero)
	diretores = models.ForeignKey(Diretores)

	def get_all(self):
		return Filmes.objects.filter().all()

	def __unicode__(self):
		return self.nome

