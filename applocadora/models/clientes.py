# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import Sum
from bairro import *

class Clientes (models.Model):

	class Meta:
		app_label = 'applocadora'

	nome = models.CharField(max_length=200)
	cpf = models.CharField(max_length=200, unique=True)
	rg = models.CharField(max_length=200, unique=True)
	telefone = models.CharField(max_length=200)
	rua = models.CharField(max_length=200)
	numero = models.CharField(max_length=200)
	cep = models.CharField(max_length=200)
	bairro = models.ForeignKey(Bairro)

	def get_all(self):
		return Clientes.objects.filter().all()

	def valor_devido(self):

		from applocadora.models.emprestimo import Filmes_clientes
		valores_devido = Clientes.objects.raw('''
			select c.id, sum(fl.valor) as valor from applocadora_clientes c
			left join applocadora_filmes_clientes fl on 
			c.id = fl.clientes_id group by c.id'''
		)

		return valores_devido

	def __unicode__(self):
		return self.nome
