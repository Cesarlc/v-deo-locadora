# -*- coding: utf-8 -*-

from django.db import models
import datetime

class Diretores (models.Model):

	class Meta:
		app_label = 'applocadora'

	nome = models.CharField(max_length=200)

	def get_all(self):
		return Diretores.objects.filter().all()

	def __unicode__(self):
		return self.nome


