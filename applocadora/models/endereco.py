# -*- coding: utf-8 -*-

from django.db import models
from bairro import *
import datetime

class Endereco (models.Model):

	class Meta:
		app_label='applocadora'

	rua = models.CharField(max_length=200)
	numero = models.IntegerField()
	cep = models.IntegerField()
	bairro = models.ForeignKey(Bairro)