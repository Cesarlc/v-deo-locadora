from django.db import models
import datetime

class Bairro (models.Model):

	class Meta:
		app_label = 'applocadora'

	nome = models.CharField(max_length=200)

	def __unicode__(self):
		return self.nome

