from applocadora.models.login import Login
from django.contrib import admin

class LoginAdmin(admin.ModelAdmin):
    
    fieldsets = [
        (None, {'fields': ['nome']}),
        ('email', {'fields': ['email']}),
        ('cpf', {'fields': ['cpf']}),
    ]

admin.site.register(Login, LoginAdmin)