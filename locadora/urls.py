#coding: utf-8

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static


from django.conf import settings

# Uncomment the next two lines to enable the admin:
import applocadora.views.home
import applocadora.views.login
import applocadora.views.diretores
import applocadora.views.genero
# import applocadora.views.filme
# import applocadora.views.clientes

urlpatterns = patterns('',
   
   # Paginas do menu { 
   
   url(r'^login/$', 
      'applocadora.views.login.autenticacao', 
      name='autenticacao'),

   url(r'^logout/$', 
      'applocadora.views.login.sair', 
      name='sair'),

   url(r'^lista_filmes/$', 
      'applocadora.views.filmes.lista_filmes', 
      name='lista_filmes'),

   url(r'^add_filmes/$', 
      'applocadora.views.filmes.add_filmes', 
      name='add_filmes'),

   url(r'^edita_filmes/(?P<id_filmes>\d+)/$', 
      'applocadora.views.filmes.edita_filmes',
      name='edita_filmes'),

   url(r'^verifica_filme/(?P<id_filmes>\d+)/$', 
      'applocadora.views.filmes.verifica_filme',
      name='verifica_filme'),

   url(r'^emprestado/$', 
      'applocadora.views.emprestimos.emprestado',
      name='emprestado'),

   url(r'^aluga_filme/$', 
      'applocadora.views.filmes.aluga_filme',
      name='aluga_filme'),

   url(r'^lista_emprestimos/$', 
      'applocadora.views.emprestimos.lista_emprestimos',
      name='lista_emprestimos'),

   url(r'^retorno_filme/(?P<id_emprestimo>\d+)/$', 
      'applocadora.views.emprestimos.retorno_filme',
      name='retorno_filme'),

   url(r'^delete_filmes/(?P<id_filmes>\d+)/$', 
      'applocadora.views.filmes.delete_filmes', 
      name='delete_filmes'),

   url(r'^lista_clientes/$', 
      'applocadora.views.clientes.lista_clientes', 
      name='lista_clientes'),

   url(r'^add_clientes/$', 
      'applocadora.views.clientes.add_clientes', 
      name='add_clientes'),

   url(r'^lista_emprestimos_divida_cliente/(?P<id_cliente>\d+)/$', 
      'applocadora.views.emprestimos.lista_emprestimos_divida_cliente',
      name='lista_emprestimos_divida_cliente'),

   url(r'^pagar/(?P<id_emprestimo>\d+)/$', 
      'applocadora.views.emprestimos.pagar',
      name='pagar'),

   url(r'^edita_clientes/(?P<id_clientes>\d+)/$', 
      'applocadora.views.clientes.edita_clientes',
      name='edita_clientes'),

   url(r'^add_bairro/$', 
      'applocadora.views.bairro.add_bairro', 
      name='add_bairro'),

   url(r'^delete_clientes/(?P<id_clientes>\d+)/$', 
      'applocadora.views.clientes.delete_clientes', 
      name='delete_clientes'),
   
   url(r'^lista_diretores/$', 
      'applocadora.views.diretores.lista_diretores', 
      name='lista_diretores'),

   url(r'^add_diretores/$', 
      'applocadora.views.diretores.add_diretores', 
      name='add_diretores'),

   url(r'^edita_diretores/(?P<id_diretores>\d+)/$', 
      'applocadora.views.diretores.edita_diretores', 
      name='edita_diretores'),

   url(r'^delete_diretores/(?P<id_diretores>\d+)/$', 
      'applocadora.views.diretores.delete_diretores', 
      name='delete_diretores'),

   url(r'^lista_genero/$', 
      'applocadora.views.genero.lista_genero', 
      name='lista_genero'),

   url(r'^add_genero/$', 
      'applocadora.views.genero.add_genero', 
      name='add_genero'),

   url(r'^edita_genero/(?P<id_genero>\d+)/$', 
      'applocadora.views.genero.edita_genero', 
      name='edita_genero'),

   url(r'^delete_genero/(?P<id_genero>\d+)/$', 
      'applocadora.views.genero.delete_genero', 
      name='delete_genero'),

   url(r'^static/(.*)$', 
      'django.views.static.serve', 
      {'document_root': settings.STATIC_ROOT}),
   
)
